<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>NeimElezi</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <!-- Styles -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css" rel="stylesheet" >
        <link href="/css/mystyle.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12 head">
                    <h2 class="titleTask">Neim Elezi - Task</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-3 categories">
                    <h4>Categories</h4>
                    <ul>
                        <li class="venues" data-city="valletta"><span>Valletta</span><span style="float:right;">+</span></li>
                        <li class="subCategories valletta">
                            <ul>
                                <li class="getCat" data-venue="valletta" data-category="toppicks"><i class="fa fa-trophy" aria-hidden="true"></i><span>Top Picks</span></li>
                                <li class="getCat" data-venue="valletta" data-category="nightlife"><i class="fa fa-glass" aria-hidden="true"></i><span>Nightlife</span></li>
                                <li class="getCat" data-venue="valletta" data-category="shopping"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span>Shopping</span></li>
                            </ul>
                        </li>
                        <li class="venues" data-city="skopje"><span>Skopje</span><span style="float:right;">+</span></li>
                        <li class="subCategories skopje">
                            <ul>
                                <li class="getCat" data-venue="skopje" data-category="coffee"><i class="fa fa-coffee" aria-hidden="true"></i><span>Coffee</span></li>
                                <li class="getCat" data-venue="skopje" data-category="nightlife"><i class="fa fa-glass" aria-hidden="true"></i><span>Nightlife</span></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 col-md-offset-1 col-sm-8 col-sm-offset-1 catResult">
                    <div id="map" title="Map">
                        <div id="mapLocation"></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $(function(){
            $( "#map" ).dialog({
                resizable: false,
                autoOpen: false,
                show: {
                    effect: "blind",
                    duration: 1000
                },
            });
            var venue = '';
            function api(section,venue){
                return "https://api.foursquare.com/v2/venues/explore?v=20171017&section="+section+"&novelty=new&near="+venue+"&client_id=NSHH33T1BZ4NBTBLLNHRYQE5DX4WG1HVT3QUO0AJQ3RUJGUA&client_secret=ED02XYA5DNRSNTJKZ5PFVTIFRPS0GGZHAZ4WUE2HQQJAJ30L"
            }
            $('.venues').on('click',function(){
                var togglethis = this.dataset.city;
                $('.'+togglethis+'').toggle('fast');
            })
            $('.getCat').on('click',function(e){
                venue = this.dataset.venue;
                $('.catResult').html('');
                var catDig = this.dataset.category;
                $.get(api(catDig,venue),{},function(response){
                    $('.catResult').append("<h4>Suggestions for "+catDig+" near "+response.response.headerLocation+"</h4>");
                    for(var j=0; j<response.response.groups[0].items.length; j++){
                        $('.catResult').append(
                            "<div class='wrapRes'><div class=\"col-md-10\"><h4>Name: "+response.response.groups[0].items[j].venue.name+"</h4></div>" +
                            "<div class=\"col-md-2 \"><h5><i class=\"fa fa-star\" aria-hidden=\"true\"></i>"+response.response.groups[0].items[j].venue.rating+"</h5></div>" +
                            "<div class=\"col-md-3 col-sm-3\"><h5><i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i>"+response.response.groups[0].items[j].venue.location.city+"</h5></div>" +
                            "<div class=\"col-md-3 col-sm-3\"><h5><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i> "+response.response.groups[0].items[j].venue.location.address+"</h5></div>" +
                            "<div class=\"col-md-3 col-sm-3\"><h5><i class=\"fa fa-globe\" aria-hidden=\"true\"></i> "+response.response.groups[0].items[j].venue.location.country+"</h5></div>" +
                            "<div class=\"col-md-3 col-sm-3\"><h5 class=\"pointer openMap\" data-lng="+response.response.groups[0].items[j].venue.location.lng+" data-lat="+response.response.groups[0].items[j].venue.location.lat+"><i class=\"fa fa-map\" aria-hidden=\"true\"></i>Map</h5></div></div>");
                    }
                    $(".openMap").on('click',function(){
                        var lat = this.dataset.lat,
                            lng = this.dataset.lng;
                        $("#mapLocation").html('<iframe width="270" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://www.openstreetmap.org/export/embed.html?bbox='+lng+'%2C'+lat+'%2C'+lng+'%2C'+lat+'&amp;layer=mapnik&amp;marker='+lat+'%2C'+lng+'" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat='+lat+'&amp;mlon='+lng+'#map=17/'+lat+'/'+lng+'" target="_blank">View Larger Map</a></small>');
                        $("#map").dialog("open");
                    })
                })
            })
        })
    </script>
</html>
